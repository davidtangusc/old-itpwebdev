Practice Practical
==================

Take each element from the list below and save them on a folder on your computer. Then recreate [this page](http://itpwebdev.usc.edu/images/practice-practical.gif) about a previous Internet World using those elements. You can download those images and text from [http://itpwebdev.usc.edu/downloads/practice-practical.zip](http://itpwebdev.usc.edu/downloads/practice-practical.zip).

I would suggest you start by putting all of those elements into one folder, such as "practice_midterm", on your computer. Then in notepad create a new file called "index.html" in that same folder. Now you can begin working on the page.

I would suggest beginning by trying to sketch out the general structure of the page first. Then perhaps build basic containers with temporary text for content (such as HEADLINE or ARTICLE BODY) and then doubling back and inserting content into the proper areas.

Also note you can open a second copy of Notepad on your computer, and then in that second copy open the text file with the article text and cut and paste pieces of the text between the two documents (the original text file and your index.html file).

The practical midterm examination will be similar in function and structure to this exercise. 

