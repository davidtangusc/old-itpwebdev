<?php

class Templates_Controller extends Base_Controller {

	public function action_home()
	{
		return View::make('partials.home', array(
			'genres' => Genre::all()
		));
	}

	public function action_about()
	{
		return View::make('partials.about');
	}

	public function action_signup()
	{
		return View::make('partials.signup');
	}

	public function action_signupFan()
	{
		return View::make('partials.signup-fan');
	}

	public function action_signupArtist()
	{
		$genres = Genre::all();
		return View::make('partials.signup-artist', array(
			'genres' => $genres
		));
	}

	public function action_projectPage()
	{
		return View::make('partials.project-page');
	}

	public function action_artistPage()
	{
		return View::make('partials.artist-page');
	}

	public function action_login()
	{
		return View::make('partials.login');
	}
}