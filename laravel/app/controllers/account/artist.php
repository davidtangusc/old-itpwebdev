<?php 

class Account_Artist_Controller extends Base_Controller
{
	protected $artist;

	public function __construct()
	{
		$this->filter('before', 'auth');
		$this->filter('before', 'artist');

		$this->artist = Artist::where('user_id', '=', Auth::user()->id)
			->where('approved', '=', 1)
			->first();
	}


	public function action_profile_template()
	{
		$profile = $this->artist->to_array();
		// dd($profile);

		return View::make('account.artist.profile', array(
			'profile' => json_encode($profile),
			'genres' => Genre::all()
		));
	}

	public function action_projects_template()
	{
		return View::make('account.artist.projects');
	}


	public function action_create_project_template()
	{
		return View::make('account.artist.create-project');
	}


	public function action_update_profile()
	{
		if (Request::ajax() AND Request::method() === 'PUT') {
			if ($this->artist->soundcloud !== Input::get('soundcloud')) {
				$sc = new Soundcloud(array(
					'client_id' => Config::get('populabel.soundcloud.client_id')
				));

				$userID = $sc->resolve('http://soundcloud.com/' . Input::get('soundcloud'))->userID;
				$this->artist->soundcloud_user_id = $userID;
			}	

			$this->artist->twitter = Input::get('twitter');
			$this->artist->soundcloud = Input::get('soundcloud');
			$this->artist->youtube = Input::get('youtube');
			$this->artist->tagline = Input::get('tagline');
			$this->artist->summary = Input::get('summary');
			$this->artist->genre_id = Input::get('genre_id');
			$this->artist->save();
		}
	}

	public function action_create_project()
	{
		if (Request::ajax() AND Request::method() === 'POST') {
			Project::create(array(
				'title' => Input::get('title'),
				'goal' => Input::get('goal'),
				'start_date' => Input::get('start'),
				'stop_date' => Input::get('stop'),
				'description' => Input::get('description'),
				'pitch_media' => Input::get('pitchMedia'),
				'pitch_media_type' => Input::get('pitchMediaType'),
				'html_media' => Input::get('htmlMedia'),
				'user_id' => $this->artist->id
			));
		}
	}

	public function action_list_projects()
	{
		if (Request::ajax()) {
			$p = array();
			$projects = Project::where('user_id', '=', $this->artist->id)->get();
			// $projects = $projects ? $projects->original : array();

			foreach($projects as $project) {
				$p[] = $project->to_array();
			}

			echo json_encode($p);
		}
	}
}