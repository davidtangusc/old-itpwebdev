<?php 

class Artists_Controller extends Base_Controller {

	public $restful = true;

	public function get_one($slug)
	{
		if (Request::ajax()) {
			$artist = Artist::where('url_slug', '=', $slug)->first()->to_array();
			$genre = Genre::where('id', '=', $artist['genre_id'])->first();

			// dd($genre);

			if ($genre) {
				$artist['genre'] = $genre->name;
			}
			
			echo json_encode($artist);
		}
	}


	public function get_all()
	{
		echo 'all artists';
	}

}