Project Proposal
===============================

1. Describe your project in a few sentences
2. Describe the APIs you'd like to use and what some of the endpoints are. 
	* Specify if you will be using JSONP or PHP to access the web service.
	* Specify what types of operations you will be performing on these API's. For example, are you only going to read data or will you do any writing/deleting/modifying of any data via an API? Note, the latter operations will require some type of authentication
3. Draw up a quick wireframe
4. Will this be more of a JS single-page application or a multi-page application/site?

You might want to look at the syllabus of topics that we haven't covered and see if any of that material might be something interesting that you'd like to incorporate.

You can submit this assignment either as a hard copy or a digital submission posted to your classpage.