<?php 

namespace WebDev\Repositories;

interface StudentRepositoryInterface {

	/**
	 * Get all students
	 * @return Array Arrayable collection
	 */
	public function all($course_number);

}