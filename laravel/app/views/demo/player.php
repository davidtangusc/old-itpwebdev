<h2>Player demo</h2>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="//connect.soundcloud.com/sdk.js"></script>

<!-- <input type="button" id="stream" value="Stream" /> -->
<input type="button" id="play" value="Play" />
<input type="button" id="pause" value="Pause" />
<input type="button" id="stop" value="Stop" />

<script>
  SC.initialize({
    client_id: '<?php echo Config::get('populabel.soundcloud.client_id') ?>'
  });

  var theSound;

  // $('#stream').on("click", function() {
    
  // });

  $('#play').on('click', function() {
    console.log(theSound);
    
    if (theSound) {
      theSound.play();  
    } else {
      SC.stream("/tracks/84767997", function(sound) {
        theSound = sound;
        sound.play();
      });
    }
  });

  $('#pause').on('click', function() {
  	theSound.pause();
  });

  $('#stop').on('click', function() {
  	theSound.stop();
  });
</script>