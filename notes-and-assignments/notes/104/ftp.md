### Uploading to the USC Server using FTP

Mac - open up Cyberduck or Fetch
PC - open FileZilla

Cyberduck, Fetch, and FileZilla are FTP (File Transfer Protocol) programs that allow you to transfer your web pages from your local hard drive to another server.

All USC students have space on the USC server. Launch the FTP program
and type in the following information:

-   Username: USC email prefix
-   Password: USC email password
-   Host: aludra.usc.edu
-   For Fetch and Cyberduck, choose SFTP (Secure FTP) from the dropdown
    menu. On FileZilla, type in 22 for the port number.

Both Filezilla (PC) and Fetch (another FTP program for Mac) are available for free download from the [USC software site](http://www.usc.edu/its/software/)