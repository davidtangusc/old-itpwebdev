<?php 

class Signup_Controller extends Base_Controller {

	public function action_process_artist()
	{
		$data = array('status' => 'Thank you for your interest in Populabel, a team member will review your application soon.');

		echo json_encode($data);
	}

	public function action_process_fan()
	{
		$validation = new Validator(Input::all(), array(
			'email' => 'required|email|unique:users',
			'password' => 'required|confirmed'
		));

		if ($validation->passes()) {
			User::create(array(
				'email' => Input::get('email'),
				'password' => Hash::make(Input::get('password')),
				'profile_type' => 1
			));

			$data = array(
				'status' => 'success',
				'message' => array(
					'Thank you for signing up. Please confirm your account by clicking on the link in your email.'
				)
			);
		} else {
			$data = array(
				'status' => 'error',
				'message' => $validation->errors->all()
			);
		}

		echo json_encode($data);
	}	

}