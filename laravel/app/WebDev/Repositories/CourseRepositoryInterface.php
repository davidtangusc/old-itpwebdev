<?php 

namespace WebDev\Repositories;

use WebDev\CourseNumber;

interface CourseRepositoryInterface {

	public function find(CourseNumber $course_number);
	public function getSchedule();
	public function getAssignments();
	public function getTime();
	public function getTAs();
	public function getInstructor();
	public function getOfficeHours();
	public function getColor();

}