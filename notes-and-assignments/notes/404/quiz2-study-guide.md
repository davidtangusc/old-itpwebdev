Quiz 2 Study Guide
==================

I will cover the following topics:

* JSONP
	* How it works
* Client side templating (nothing specific to Handlebars)
	* Why use them?
	* How to create a template using script tags
* Asynchronous requests
	* What are they?
	* How do they work?
	* How do they affect the user experience?
	* Types of asynchronous requests (JSONP, AJAX)
* AJAX
	* What is it?
	* The function that is used to make AJAX requests in __plain__ JS
* The promise pattern
	* why use it?
* jQuery's .data() method

The quiz will be multiple choice and T/F.

