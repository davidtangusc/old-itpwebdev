<?php 

class Account_Fan_Controller extends Base_Controller
{
	public function __construct()
	{
		$this->filter('before', 'auth');
		$this->filter('before', 'fan');
	}


	public function action_profile_template()
	{
		return View::make('account.fan.profile');
	}
}