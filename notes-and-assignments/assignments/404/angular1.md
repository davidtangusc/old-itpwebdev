Angular.js Part 1
=================

Create an application similar to what we did during the class demo (itunes search application) but use a different JSONP request or use an AJAX request to some PHP script.

Requirements:

* Create an angular module
* attach a controller to that module
* Use angular's $http service to interact with your api or php script

