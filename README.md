#### Requirements

* PHP 5.3
* composer

#### Steps to get up and running

1. clone the repository
2. cd laravel
3. composer install
4. cd ..
5. chmod -R 777 laravel/app/storage
6. Run ./build.
7. create a virual host that points to laravel/public/index.php. My virtual host was webdev.local.com. See [http://davidwalsh.name/create-virtual-host](http://davidwalsh.name/create-virtual-host) for steps on how to do that. I have also included the steps that I took on my mac below as well.
8. Visit your local domain and you should be up and running

#### Setting up a virtual host (on a mac)

##### Edit httpd-vhosts.conf File

```
cd /private/etc/apache2/extra/
sudo vim httpd-vhosts.conf
```

Add this entry

```
<VirtualHost *:80>
  DocumentRoot "/Users/dtang85/Sites/webdev/laravel/public"
  DirectoryIndex index.php
  ServerName webdev.local.com
  <Directory "/Users/dtang85/Sites/webdev/laravel/public">
    AllowOverride All
    Allow from All
  </Directory>
</VirtualHost>
```

##### Edit hosts file

```
sudo vim /private/etc/hosts
```

Add the following line:

```
127.0.0.1 webdev.local.com
```

##### Restart Apache

```
apache restart
// OR
apachectl
```

### Files to edit
* laravel/app/config/{{COURSE NUMBER}}.json
* notes-and-assignments repository
* laravel/public/students
	* json file created by chrome extension. see ITP-Webdev repository
	* save off web page images by chosing Save page as on mac and it will save all the page's assets. images will have a file extension of jsp but it sill works fine.
