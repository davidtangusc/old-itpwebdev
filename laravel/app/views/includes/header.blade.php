<header>
	<h2>
		<a href="<?php echo url('/' . $course_number) ?>">ITP {{ $course_number }} - {{ $course->name }}</a>
	</h2>
	<p>
		<strong>Instructor:</strong>
		<span>{{ $course->getInstructor() }}</span>
	</p>
	<p>
		<strong>Time/Location:</strong>
		<span>{{ $course->getTime() }},</span>
		<a href="{{ $course->syllabus }}" target="_blank">Syllabus</a>
	</p>
	<p>
		<strong>TA's:</strong>
		@foreach($course->getTAs() as $ta)
			<a href="mailto:{{ $ta->email }}">{{ $ta->name }}</a>
		@endforeach
	</p>
	<p>
		<strong>Office Hours:</strong>
		<span>{{ $course->getOfficeHours() }}</span>
	</p>
</header>