<?php 

namespace WebDev\Repositories;

class JsonStudentRepository implements StudentRepositoryInterface {

	public function all($course_number)
	{
		$path = dirname(__FILE__) . "/../../../public/students/$course_number.json";

		if (file_exists($path)) {
			$contents = file_get_contents($path);
			return json_decode($contents);
		}

		return array();
	}


}