<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/404.html', function() {
	echo '404 - page not found';
});

/**
 * Class chooser page
 */
Route::get('/', function()
{
	return View::make('home');
});

/**
 * Specific class home page
 */
Route::get('/{course_number}', function($course_slug)
{
	$course_number = App::make('WebDev\CourseNumber');
	$course_number->set($course_slug);

	$courses = App::make('\WebDev\Repositories\CourseRepositoryInterface');
	$course = $courses->find($course_number);

	$students = App::make('\WebDev\Repositories\StudentRepositoryInterface');
	$students = $students->all($course_number);

	return View::make('layouts.main', array(
		'course_number' => $course_number,
		'course' => $course
	))->nest('content', 'partials.schedule', array(
		'course_number' => $course_number,
		'homeworks' => $course->getAssignments(),
		'weeks' => $course->getSchedule(),
		'students' => $students,
	));
});

/**
 * A markdown resource for a given class from the notes-and-assignments repository
 */
Route::get('/{course_number}/{category}/{file}', function($course_slug, $category, $file)
{
	$cache_name = implode('_', array($course_slug, $category, $file));
	$html = Cache::get($cache_name);
	$course_number = App::make('WebDev\CourseNumber');
	$course_number->set($course_slug);

	if (!$html) {
		$course_digits = $course_number->getNumeric();
		$path = dirname(__FILE__) . "/../../notes-and-assignments/$category/$course_digits/$file.md";

		if (file_exists($path)) {
			$contents = file_get_contents($path);
		} else {
			return Redirect::to('/' . $course_number);
		}

		$github = App::make('WebDev\API\GitHub');
		$html = $github->setMarkdown($contents)->fetchHTML();

	  Cache::put($cache_name, $html, 10080);
	}

  $courses = App::make('\WebDev\Repositories\CourseRepositoryInterface');
	$course = $courses->find($course_number);

	return View::make('layouts.main', array(
		'course_number' => $course_number,
		'course' => $course,
		'content' => $html
	));
});