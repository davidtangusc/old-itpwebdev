<sidebar>
	<h3 class="block-title">Assignments</h3>
	<ol>
		@foreach($homeworks as $hw)
			@if ($hw->active === 1)
				@if ($hw->type === 'project')
					<li style="background-color: yellow;">
				@else
					<li>
				@endif
					<a href="./{{ $course_number }}/assignments/{{ $hw->link }}">
						{{ $hw->name }}
						@if ($hw->type == 'assignment')
							(A)
						@else
							(L)
						@endif
					 </a> 
					<span>due {{ $hw->due }}</span>
				</li>
			@endif
		@endforeach
	</ol>

  <h3 class="block-title">Resources</h3>
  <ul>
      <li><a href="{{ asset('resources/vlab_instructions_091013.pdf') }}" target="_blank">VLab instructions</a></li>
  </ul>

	<h3 class="block-title">Students</h3>
	@foreach($students as $student)
		<div class="row">
			<div class="col-lg-12 student">
			@if (in_array($course_number, array('300a', '300b')))
				<a href="http://itp300.usc.edu/{{ $student->username }}/classpage.php" target="_blank">
			@else
				<a href="http://scf.usc.edu/~{{ $student->username }}/itp{{ $course_number }}/classpage.html" target="_blank">
			@endif
					<img src="./students/{{ $course_number }}/{{ $student->img }}" alt="{{ $student->name }}" width="50">
					<span>{{ $student->name }}</span>
				</a>
			</div>
		</div>
	@endforeach
</sidebar>