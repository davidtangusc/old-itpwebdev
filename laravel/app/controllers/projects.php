<?php 

class Projects_Controller extends Base_Controller {
	public $restful = true;

	public function get_one()
	{
		$project = Project::find(Input::get('id'));

		if ($project) {
			$project = $project->to_array();
		}

		echo json_encode($project);
	}

}