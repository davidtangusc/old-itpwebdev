<!DOCTYPE html>
<html>
<head>
	<title>ITP</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo asset('css/normalize.css') ?>">
	<link rel="stylesheet" href="<?php echo asset('vendor/bootstrap/css/bootstrap.css') ?>">
</head>
<body>
	<div class="container">
		<h1>ITP Webdev</h1>
		<p>Please choose a course to visit the class site.</p>
		<ul>
			<li><a href="./104">ITP 104 - Web Publishing</a></li>
			<li><a href="./300a">ITP 300a - Database Web Development (David)</a></li>
			<li><a href="./300b">ITP 300b - Database Web Development (Yuanbo)</a></li>
			<li><a href="./404">ITP 404 - Modern Technologies in Web Development</a></li>
		</ul>
	</div>
</body>
</html>