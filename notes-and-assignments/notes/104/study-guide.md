Study Guide
===========

<ul>
	<li> <strong>HTML basics:</strong> Fundamentals of how HTML tags work. Tags, attributes and attribute properties. Basic format for tags and attributes. Using HTML tags to organize text and create basic objects such as lines and images. <em>See <a href="http://webdev.usc.edu/component.cfm?compjoinid=3827&amp;course=104t">Basic HTML</a>, <a href="http://webdev.usc.edu/component.cfm?compjoinid=3828&amp;course=104t">Basic HTML Tags</a>, and "<a href="http://webdev.usc.edu/component.cfm?compjoinid=3843&amp;course=104t">good code</a>" (portion of lecture).</em></li>
    <br> 
	<li><strong>CSS Style Properties</strong> and Stylesheets: Almost all formatting of text and objects should be done through style properties. Basics of opening up style attribute blocks in HTML tags. Syntax of style instructions. <em>See "<a href="http://webdev.usc.edu/component.cfm?compjoinid=3843&amp;course=104t">styles</a>" (portion of lecture), "<a href="http://webdev.usc.edu/component.cfm?compjoinid=3868&amp;course=104t">stylesheets</a>" (portion of lecture) and <a href="http://webdev.usc.edu/component.cfm?compjoinid=3844&amp;course=104t">CSS Properties</a>.</em> Note: Are responsible for being able to implement/use styles but NOT expected to memorize all style property names. We also covered using stylesheets to set up clases and IDs (as well as redefine html tags) in the stylesheets and styles section of the div's cont lecture. <br>
 
  </li><li><strong>Create layouts and using div tags</strong>: How to use "div" tags along with specific style properties such as margin, padding, float, width and height to create "boxes" that make up a page's design and layout. <em>See <a href="http://webdev.usc.edu/component.cfm?compjoinid=3855&amp;course=104t">Layout with divs</a>, "<a href="http://webdev.usc.edu/component.cfm?compjoinid=3868&amp;course=104t">Divs and layouts (con't)</a>" (portion of lecture), and the <a href="http://webdev.usc.edu/component.cfm?compjoinid=3892&amp;course=104t">layout section</a> from the forms lecture, and finally the <a href="http://webdev.usc.edu/component.cfm?compjoinid=3876&amp;course=104t">CSS portion</a> of the "fun css" lecture. Also review the samples from <a href="http://webdev.usc.edu/component.cfm?compjoinid=3860&amp;course=104t">Some layout templates</a> and <a href="http://webdev.usc.edu/component.cfm?compjoinid=3872&amp;course=104t">Calendar templates</a>.</em></li>
  <br> 
	<li> <strong>Form objects:</strong> HTML tags used to create a range of form objects from text boxes to radio and checkboxes to drop-down/select menus to buttons. Understanding of the different tags and syntax. <em>See "<a href="http://webdev.usc.edu/component.cfm?compjoinid=3892&amp;course=104t">forms</a>" (portion of lecture and <a href="http://webdev.usc.edu/component.cfm?compjoinid=3896&amp;course=104t">Form Quicksheet</a>.</em></li>
    <br> 
</ul>

<br>
      <strong>Review of ITP104 Content:</strong>
<ul>
<li><em>Knowledge of HTML tags:</em> You should be familiar and able to use (without notes) all "basic" html tags AND form tags.
You should be able to answer questions about tag functionality. <br>
 
</li><li> <em>Using HTML tags:</em> You should be able to create a web page (without notes) of similar complexity to your previous assignments and the practice exam.<br>
 
</li><li><em>Using Styles:</em> You should be familiar with the basic mechanics of setting style properties, of creating tag, id and class styles, and be able to use any style property (if provided with a list) to apply formatting to objects. 
<br>
 
</li><li><em>Stylesheets, classes and IDS:</em>  You should know how to set up a "style" block in the head of a document and to create named ID or class entries for associating style instructions to objects.
<br>
 </li><li><em>Making page layouts:</em> You should be able to create web page layouts using div tags for a variety of formats or uses, again similar in scope and complexity to the pages you created in your labs and assignments thus far.<br>
   
</li><li><em> Creating form objects:</em> You should be able to use HTML tags to create common form objects. 
</li></ul>
