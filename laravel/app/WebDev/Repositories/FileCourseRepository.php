<?php 

namespace WebDev\Repositories;

use WebDev\CourseNumber;

class FileCourseRepository implements CourseRepositoryInterface {

	public $name;
	public $instructor;
	public $time;
	public $homework;
	public $weeks;
	public $syllabus;
	public $ta;
	public $office_hours;
	public $color;


	public function find(CourseNumber $course_number)
	{
		$path = dirname(__FILE__) . '/../../config/' . $course_number . '.json';

		// dd($path);

		if (file_exists($path)) {
			$contents = file_get_contents($path);
			$json = json_decode($contents); // fetch the right config json file

			$this->name = $json->course_name;
			$this->time = $json->time;
			$this->homework = $json->homework;
			$this->weeks = $json->weeks;
			$this->syllabus = $json->syllabus;
			$this->instructor = $json->instructor;
			$this->ta = $json->ta;
			$this->office_hours = $json->office_hours;
			$this->color = $json->color;
		} else {
			throw new \Exception('Course not found');
		}

		return $this;
	}

	public function getColor()
	{
		return $this->color;
	}

	public function getSchedule()
	{
		return $this->weeks;
	}

	public function getAssignments()
	{
		return $this->homework;
	}

	public function getTime()
	{
		return $this->time;
	}

	public function getTAs()
	{
		return $this->ta;
	}

	public function getInstructor()
	{
		return $this->instructor;
	}

	public function getOfficeHours()
	{
		return $this->office_hours;
	}

}