Project Requirements
====================

Your final project will be a web application mashup on a topic of interest to you. You can visit [Programmable Web](http://programmableweb.com) to get different ideas about what types of APIs you'd like to use and how might you integrate them to create a unique experience.

### Final Project Requirements

* You must use __at least__ 2 web APIs. You can use: 
	* JSONP calls is available
	* make the call from PHP
	* 3rd party JavaScript APIs like Google Maps and Parse (data storage). This should involve more than embedding some code on your page
* Your project should be styled so that it looks organized and professional, and it is easy to use. For quick styling, you can use Twitter Bootstrap

### Project Extras (Choose at least 2 of the following)

* Caching API calls made on the server
* Angular.js
* Client-side templating via Handlebars, Underscore, etc
* Geolocation
* Additional API integration (more than the required 2)
* AJAX

### Other APIs you might want to explore

* [Google Charting API](https://developers.google.com/chart/)

### Project Submission

* Your project can be turned in several ways:
	1. Hosted on aludra and a link to it is on your classpage
	2. Deployed to a Platform as a Service (PaaS) like Appfog, Heroku, or Fort Rabbit. This would be if you are using a server-side technology that aludra does not support (Ruby, etc). Post a link to your hosted project on your class page.
	3. Posted to Cloud 9 and give itpweb user RW access. Then post a link to the Cloud 9 project on your class page.

__A zip file of all your code should be linked to your classpage so that I can download it__

I will also be grading your project on the following criteria:

* Code organization (since we discussed many approaches to writing maintainable, organized code). However, I would rather see a working application than a broken one that attempts to have best practices. You can organize your application in several ways such as:
	* application namespacing for various modules in your application (this is if you aren't using Angular.js)
	* use Angular.js and separate everything out into controllers, services, directives, distinct modules, etc
* Overall complexity. It should be more complex than your assignments and midterm project and your APIs should be integrated in some way
* User experience / styling