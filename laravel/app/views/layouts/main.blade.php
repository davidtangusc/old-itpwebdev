<!doctype html>
<html lang="en">
<head>
	<title>ITP WebDev</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo asset('css/normalize.css') ?>">
	<link rel="stylesheet" href="<?php echo asset('vendor/bootstrap/css/bootstrap.css') ?>">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo asset('css/github.css') ?>">
	<link rel="stylesheet" href="<?php echo asset('css/main.css') ?>">
</head>
<body>

<div class="bar" style="background: {{ $course->getColor() }};">
    <ul class="itp-external-links">
        <li><a href="{{ url('/') }}">
          <i class="fa fa-home fa-lg"></i>
        </a></li>
        <li><a href="https://www.facebook.com/USCViterbiITP" target="_blank">
          <i class="fa fa-facebook fa-lg"></i>
        </a></li>
        <li><a href="https://twitter.com/uscitp" target="_blank">
          <i class="fa fa-twitter fa-lg"></i>
        </a></li>
        <li><a href="https://www.youtube.com/channel/UCLhLYSysw4juedNhsXGo7Vw?feature=watch" target="_blank">
          <i class="fa fa-youtube fa-lg"></i>
          Screencasts
        </a></li>
    </ul>

</div>
<div class="container">
	@include('includes.header')	

	{{ $content }}

	@include('includes.footer')
</div><!-- #container -->

<!-- <script src="https://cdn.firebase.com/v0/firebase.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
<script src="<?php echo asset('js/app.js') ?>"></script> -->


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43150820-1', 'af.cm');
  ga('send', 'pageview');

</script>

</body>
</html>