<?php 

namespace WebDev\API;

class GitHub {

	protected $markdown;

	public function setMarkdown($markdown)
	{
		$this->markdown = $markdown;

		return $this;
	}

	public function fetchHTML()
	{
		$data = array(
			'text' => $this->markdown,
			'mode' => 'gfm'
		);

		$data = json_encode($data);

		$options = array(
	    CURLINFO_HEADER_OUT => true,
	    CURLOPT_HTTPHEADER => array(
	    	'User-Agent: skaterdav85'
	    ),
	    CURLOPT_URL => 'https://api.github.com/markdown',
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_POST => true,
	    CURLOPT_POSTFIELDS => $data,
	  );

		$ch = curl_init();
	  curl_setopt_array($ch, $options);
	  $this->html = curl_exec($ch);
	  $info = curl_getinfo($ch);
	  curl_close($ch);

	  // var_dump($info);
	  // var_dump($this->html);
	  return $this->html;
	}

}