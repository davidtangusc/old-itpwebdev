<?php 

namespace WebDev\Repositories;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    public function register()
    {
        \App::bind('\WebDev\Repositories\CourseRepositoryInterface', '\WebDev\Repositories\FileCourseRepository');
				\App::bind('\WebDev\Repositories\StudentRepositoryInterface', '\WebDev\Repositories\JsonStudentRepository');
    }

}