<?php 

namespace WebDev;

/**
 * Used to parse a course number because a course number can be numeric only (104) or contain letters in it (300m)
 */

class CourseNumber {
	
	protected $course_number;
	protected $course_digits;

	public function set($course_number)
	{
		$this->course_number = $course_number;
		$result = preg_match("/\d+/", $course_number, $matches);

		if ($result === 1) {
			$this->course_digits = $matches[0];
		}
	}

	public function getNumeric()
	{
		return $this->course_digits;
	}

	public function __toString()
	{
		return $this->course_number;
	}



}