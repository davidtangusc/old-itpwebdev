These chapters cover creating calculated fields, inserting (rows of) data, updating data, deleting data, and creating views. 

You will need to use many of the SQL constructs in the reading for your homework.  