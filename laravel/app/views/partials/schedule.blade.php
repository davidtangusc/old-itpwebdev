<div class="row">
	<div class="col-lg-7">
		<h3 class="block-title">Lectures</h3>
		<div>
			@foreach($weeks as $week)
				@if($week->active === 1)
					<article class="week">
						<p style="font-style: italic; font-size: 11px; text-align: right;">{{ $week->date }}</p>
						@foreach($week->units as $unit)
							<div>
								@if($unit->link)
									<?php 
										if (is_int( strpos($unit->link, 'http://') ))
											$url = $unit->link;
										else 
											$url = "./$course_number/notes/" . $unit->link;
									?>
									<a href="{{ $url }}">{{ $unit->subject }}</a>
								@else
									{{ $unit->subject }}
								@endif
								
							</div>
						@endforeach
					</article>
				@endif
			@endforeach
		</div>

		<h3>Open Labs</h3>
		@include('includes.open-labs')
	</div>

	<div class="col-lg-5">
		@include('includes.sidebar')
	</div>
</div>