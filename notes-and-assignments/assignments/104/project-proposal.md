Write up a proposal for your individual project to turn in. Your proposal must include:

* Graphically rendered/drawn __site/flow chart__ for your project.
* Section on __Mission__ of the site.
* Section on __Audience's__ for the site.
* Section on __Content__ for the site.
* Section on __Graphics__ for the site. Are the graphics created in Photoshop, use of images, icons from the web, etc?
* Notes on overall __design__ and __style__ of the site.
* Some idea of the size/scope of the site, including an approximate __page count__.

You can either submit a hard copy or submit electronically by posting a link to a PDF, MS Word, or PPT file on your class page.

[Site Map Example](http://itpwebdev.usc.edu/images/site_map.jpg)
